package Main;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.Connection;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import controller.ProgramaController;
import view.vistaDeleteCliente;
import view.vistaDeleteVideo;
import view.vistaInsertarCliente;
import view.vistaInsertarVideo;
import view.vistaPrincipal;
import view.vistaUpdateCliente;
import view.vistaUpdateVideo;

public class mainApp extends JFrame {
	
	public static Connection conexion;
	
	vistaPrincipal miVistaPrincipal;
	
	vistaInsertarCliente miVistaInsertarCliente;
	vistaInsertarVideo miVistaInsertarVideo;
	
	vistaUpdateCliente miVistaUpdateCliente;
	vistaUpdateVideo miVistaUpdateVideo;
	
	vistaDeleteCliente miVistaDeleteCliente;
	vistaDeleteVideo miVistaDeleteVideo;
	
	ProgramaController programaController;
	//Aquí ejecutaremos el método que inicializará todo el programa
	public static void main(String[] args) {
		
		mainApp miPrograma=new mainApp();
		miPrograma.iniciar();
		
	}
	//Aquí cargo todas las vistas y las enlazo entre ellas
	private void iniciar() {

		
		miVistaPrincipal=new vistaPrincipal();
		
		miVistaInsertarCliente=new vistaInsertarCliente();
		miVistaInsertarVideo=new vistaInsertarVideo();
		
		miVistaUpdateCliente= new vistaUpdateCliente();
		miVistaUpdateVideo= new vistaUpdateVideo();
		
		miVistaDeleteCliente= new vistaDeleteCliente();
		miVistaDeleteVideo= new vistaDeleteVideo();
	
		programaController= new ProgramaController();
		
		programaController.setMiVistaPrincipal(miVistaPrincipal);
		programaController.setMiVistaInsertarCliente(miVistaInsertarCliente);
		programaController.setMiVistaInsertarVideo(miVistaInsertarVideo);
		
		programaController.setMiVistaUpdateCliente(miVistaUpdateCliente);
		programaController.setMiVistaUpdateVideo(miVistaUpdateVideo);
		
		programaController.setMiVistaDeleteCliente(miVistaDeleteCliente);
		programaController.setMiVistaDeleteVideo(miVistaDeleteVideo);
		//Hago que la vista de la ventana principal siempre sea visible	
		miVistaPrincipal.setVisible(true);


	}


}
