package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.ProgramaController;
import dto.UD22Dto;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

//vista del menú que será usado para insertar un cliente
public class vistaInsertarCliente extends JFrame{

	private JPanel contentPane;
	private JTextField textFieldid;
	private JLabel lblNombre;
	private JTextField textFieldnombre;
	private JLabel lblApellido;
	private JTextField textFieldapellido;
	private JLabel lblDireccion;
	private JTextField textFielddireccion;
	private JLabel lblDNI;
	private JTextField textFielddni;
	private JLabel lblFecha;
	private JTextField textField;
	private JLabel lblNewLabel;
	private JButton btnInsertar;
	
 public vistaInsertarCliente() {
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setTitle("Insertar Cliente");
		
		JLabel lblid = new JLabel("ID");
		lblid.setBounds(25, 69, 32, 14);
		contentPane.add(lblid);
		
		textFieldid = new JTextField();
		textFieldid.setBounds(75, 66, 86, 20);
		contentPane.add(textFieldid);
		textFieldid.setColumns(10);
		
		lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(25, 105, 45, 14);
		contentPane.add(lblNombre);
		
		textFieldnombre = new JTextField();
		textFieldnombre.setColumns(10);
		textFieldnombre.setBounds(75, 102, 86, 20);
		contentPane.add(textFieldnombre);
		
		lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(25, 141, 45, 14);
		contentPane.add(lblApellido);
		
		textFieldapellido = new JTextField();
		textFieldapellido.setColumns(10);
		textFieldapellido.setBounds(75, 138, 86, 20);
		contentPane.add(textFieldapellido);
		
		lblDireccion = new JLabel("Direccion");
		lblDireccion.setBounds(191, 69, 60, 14);
		contentPane.add(lblDireccion);
		
		textFielddireccion = new JTextField();
		textFielddireccion.setColumns(10);
		textFielddireccion.setBounds(261, 66, 86, 20);
		contentPane.add(textFielddireccion);
		
		lblDNI = new JLabel("DNI");
		lblDNI.setBounds(191, 105, 60, 14);
		contentPane.add(lblDNI);
		
		textFielddni = new JTextField();
		textFielddni.setColumns(10);
		textFielddni.setBounds(261, 102, 86, 20);
		contentPane.add(textFielddni);
		
		lblFecha = new JLabel("Fecha");
		lblFecha.setBounds(191, 141, 60, 14);
		contentPane.add(lblFecha);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(261, 138, 86, 20);
		contentPane.add(textField);
		
		lblNewLabel = new JLabel("Insertar cliente");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblNewLabel.setBounds(146, 11, 137, 34);
		contentPane.add(lblNewLabel);
		
		btnInsertar = new JButton("Insertar");
		btnInsertar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UD22Dto miCliente=new UD22Dto();
				miCliente.setIdcliente(Integer.parseInt(textFieldid.getText()));
				miCliente.setNombre(textFieldnombre.getText());
				miCliente.setApellido(textFieldapellido.getText());
				miCliente.setDireccion(textFielddireccion.getText());
				miCliente.setDni(textFielddni.getText());
				miCliente.setFecha(Integer.parseInt(textField.getText()));
				
				ProgramaController.registrarCliente(miCliente);
			}
		});
		btnInsertar.setBounds(119, 194, 89, 23);
		contentPane.add(btnInsertar);
		
	}
 
	
//	public void actionPerformed(ActionEvent e) {
//		if (e.getSource()==btnInsertar)
//		{
//			try {
//	
//			} catch (Exception ex) {
//				JOptionPane.showMessageDialog(null,"Error en el Ingreso de Datos","Error",JOptionPane.ERROR_MESSAGE);
//				System.out.println(ex);
//			}
//		}
//	}


 
}
