package controller;

import dto.*;
import service.*;
import view.*;

public class ProgramaController {
	
	
	private vistaPrincipal miVistaPrincipal;
	
	private static vistaInsertarCliente miVistaInsertarCliente;
	private static vistaInsertarVideo miVistaInsertarVideo;
	
	private static vistaUpdateCliente miVistaUpdateCliente;
	private static vistaUpdateVideo miVistaUpdateVideo;
	
	private vistaDeleteCliente miVistaDeleteCliente;
	private static vistaDeleteVideo miVistaDeleteVideo;
	
	//Aquí crearé todos los getters y todos los setters de las vistas de mi programa

	public vistaPrincipal getMivistaPrincipal() {
		return miVistaPrincipal;
	}
	public void setMiVistaPrincipal(vistaPrincipal miVistaPrincipal) {
		this.miVistaPrincipal = miVistaPrincipal;
	}
	
	public vistaInsertarCliente getMiVistaInsertarCliente() {
		return miVistaInsertarCliente;
	}
	public void setMiVistaInsertarCliente(vistaInsertarCliente miVistaInsertarCliente) {
		this.miVistaInsertarCliente = miVistaInsertarCliente;
	}
	public vistaInsertarVideo getMiVistaInsertarVideo() {
		return miVistaInsertarVideo;
	}
	public void setMiVistaInsertarVideo(vistaInsertarVideo miVistaInsertarVideo) {
		this.miVistaInsertarVideo = miVistaInsertarVideo;
	}
	
	public vistaUpdateCliente getMiVistaUpdateCliente() {
		return miVistaUpdateCliente;
	}
	public void setMiVistaUpdateCliente(vistaUpdateCliente miVistaUpdateCliente) {
		this.miVistaUpdateCliente = miVistaUpdateCliente;
	}
	public vistaUpdateVideo getMiVistaUpdateVideo() {
		return miVistaUpdateVideo;
	}
	public void setMiVistaUpdateVideo(vistaUpdateVideo miVistaUpdateVideo) {
		this.miVistaUpdateVideo = miVistaUpdateVideo;
	}
	
	public void setMiVistaDeleteCliente(vistaDeleteCliente miVistaDeleteCliente) {
		this.miVistaDeleteCliente = miVistaDeleteCliente;
	}
	public vistaDeleteCliente getMiVistaDeleteCliente() {
		return miVistaDeleteCliente;
	}
	public void setMiVistaDeleteVideo(vistaDeleteVideo miVistaDeleteVideo) {
		this.miVistaDeleteVideo = miVistaDeleteVideo;
	}
	public vistaDeleteVideo getMiVistaDeleteVideo() {
		return miVistaDeleteVideo;
	}
	
	//Aquí hago visible las vistas de mi aplicación
	public static void mostrarVistaInsertarCliente() {
		miVistaInsertarCliente.setVisible(true);
	}
	public static void mostrarVistaInsertarVideo() {
		miVistaInsertarVideo.setVisible(true);
	}
	
	public static void mostrarVistaUpdateCliente() {
		miVistaUpdateCliente.setVisible(true);
	}
	public static void mostrarVistaUpdateVideo() {
		miVistaUpdateVideo.setVisible(true);
	}
	
	public static void mostrarVistaDeleteCliente() {
		miVistaDeleteVideo.setVisible(true);
	}
	
	public static void mostrarVistaDeleteVideo() {
		miVistaDeleteVideo.setVisible(true);
	}
	
	//Aquí utilizo el controller para enviar la información de la view al service
	public static void registrarCliente(UD22Dto miCliente) {
		ProgramaServ.validarRegistroCliente(miCliente);
	}
	
	public static void registrarVideo(UD22Dto miVideo) {
		ProgramaServ.validarRegistroVideo(miVideo);
	}
	
	public static void modificarCliente(UD22Dto miCliente) {
		ProgramaServ.validarModificacionCliente(miCliente);
	}
	public static void modificarVideo(UD22Dto miVideo) {
		ProgramaServ.validarModificacionVideo(miVideo);
	}
	public static void eliminarCliente(UD22Dto miClienteDelete) {
		ProgramaServ.validarEliminacionCliente(miClienteDelete);
	}
	public static void eliminarVideo(UD22Dto miVideoDelete) {
		ProgramaServ.validarEliminacionVideo(miVideoDelete);
	}

	
	

}
