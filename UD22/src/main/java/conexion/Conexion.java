package conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {
		//Aquí asignaremos los campos necesarios para realizar una conexión a nuestra base de datos
	   static String bd = "bd";
	   static String login = "user";
	   static String password = "pass";
	   static String url = "jdbc:mysql://localhost:3306/"+bd+"?useTimezone=true&serverTimezone=UTC";

	   Connection conn = null;

	   // Aquí estableceremos la conexión a nuestra base de datos
	   public Conexion() {
	      try{
	         //Aquí asigno el driver sql que usaremos
	         Class.forName("com.mysql.cj.jdbc.Driver");
	         conn = DriverManager.getConnection(url,login,password);

	         if (conn!=null){
	            System.out.print("Conexión a base de datos "+bd+"_SUCCESS at");
	         }
	      }
	      catch(SQLException e){
	         System.out.println(e);
	      }catch(ClassNotFoundException e){
	         System.out.println(e);
	      }catch(Exception e){
	         System.out.println(e);
	      }
	   }
	   public Connection getConnection(){
	      return conn;
	   }

	   public void desconectar(){
	      conn = null;
	   }
	   

	}

